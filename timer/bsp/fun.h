#ifndef __FUN_H__
#define __FUN_H__

#include "stm32g4xx.h"

void led_show(uint8_t led, uint8_t mode);
void key_scan(void);
void lcd_show(void);


#endif // __FUN_H__
